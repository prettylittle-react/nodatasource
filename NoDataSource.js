import React from 'react';
import PropTypes from 'prop-types';

import './NoDataSource.scss';

/**
 * NoDataSource
 * @description [Description]
 * @example
  <div id="NoDataSource"></div>
  <script>
    ReactDOM.render(React.createElement(Components.NoDataSource, {
        title : 'Example NoDataSource'
    }), document.getElementById("NoDataSource"));
  </script>
 */
class NoDataSource extends React.Component {
	constructor(props) {
		super(props);

		this.baseClass = 'nodatasource';
	}

	render() {
		return (
			<div className={`${this.baseClass}`} ref={component => (this.component = component)}>
				NoDataSource
			</div>
		);
	}
}

NoDataSource.defaultProps = {
	children: null
};

NoDataSource.propTypes = {
	children: PropTypes.node
};

export default NoDataSource;
